<?php

echo "Hello world \n";

/* no terminal, 
    php [nome do arquivo]
para rodar */


/* fechar a tag do php é opcional,
recomendado não ter

?>

*/

//////////////////////////////////
/*

Iniciar repositório
    git init

Adicionar arquivos (. = tudo)
    git add .

Commitar
    git commit -m "explicação do commit"

Ver registros
    git log

Alterar commit recém-feito
    git commit --amend

Adicionando remotos
    git remote add origin git@gitlab.com:[repositório]
    git remote add origin [url do repositório]

Gerando chave SSH
    ssh-keygen -t rsa -b 2048

Enviando para repositório
    git pull [remote] [branch]

