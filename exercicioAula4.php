<?php
// Escrever algum trecho do código do projeto de vocês
// O código precisa ser algo funcional
// Os nomes das funções/variáveis/parâmetros devem deixar clara a intenção do código

$input = "html < tag";

// for demonstration purposes
function escape($string, $char) {
    for ($i = 0; $i < strlen($string); $i++) {
        if ($string[$i] == "$char") {
            //splits in two
            $before = substr($string, 0, $i); // stops before char
            $after = substr($string, $i); // starts on char

            // insert
            $string = $before."\\".$after;

            // advance 1 to cover insert
            $i++;
        };
    };

    return $string;
};

// function proper
function sanitize($userInput, $mode) {
    $sanitizedString = $userInput;
    $warning = false;
    $report = [];

    $tests = [
        [0, "<", 0], // open code
        [1, ">", 0], // close code
        [2, "/", 0], // close code
        [3, "js", 0], // javascript
        [4, '"', 0] // close string
    ];

    for ($i = 0; $i < count($tests); $i++) {
        if (str_contains($sanitizedString, $tests[$i][1])) {
            $tests[$i][2] = 1;
            $warning = true;
            $report[$i] = "Potentially dangerous character found: ".$tests[$i][1]." '";

            $sanitizedString = escape($sanitizedString, $tests[$i][1]);
        } else {
            $report[$i] = "OK";
        };
    };

    // save test report in database
    // and present if param MODE = 1
    if ($mode == 1) {
        echo("Pretend it is connected to DB: \n");
        echo("{\n");
        foreach ($tests as $test) {
            echo ("  'test".$test[0]."': { \n    'target': '".$test[0]."', \n    'result': '".$report[$test[0]]."\n  },\n");
        };
        echo("}");

        return null;
    }

    return $sanitizedString;
};


echo(sanitize($input, 1));

echo("\n \nAfter sanitization: \n");
echo (sanitize($input, 0));

echo("\n \n");

