<?php
$title = 'Titulo da Página';
$body = 'Hello World';
?>

<html>
<head>
    <title><?php echo $title; ?></title>
</head>
<body>
    <h1><?php echo $body; ?></h1>
</body>
</html>

<!----

Servidor embutido do PHP
    php -S 0.0.0.0:8000

Daí dá para acessar pelo navegador e complementar a URL com /[nome do arquivo]
CTRL+U mostra o original

---->

<?php

// variáveis: começam com $ / tem que ser entendível, compreensível e precisa
// tipagem: bool, int, float, string

$number = 1;
echo $number * 3, "\n";
$textNumber = '1';
echo $textNumber * 3, "\n";
$text = 'Hello World';
echo $text * 3;

// OUTPUT:
// 3
// 3
// PHP Warning:  A non-numeric value encountered in D:\UNIMAR\teste.php on line 11
// 0

// ponteiros: $var; $pointer = &$var
//      passou o endereço da variável

// convenções:
//      camelCase
//      PascalCase
//      snake_case
//      SCREAMING_SNAKE_CASE
//      kebab-case
//      SCREAMING-KEBAB-CASE

$array = array();
var_dump($array); // antigo

$array = [];
var_dump($array); // novo

$array = [
    1,
    'bar',
    ['outro array']
]

$array = [
    0 => 1,
    1 => 'bar',
    2 => ['outro']
]

$array = [
    'a' => 1,
    'b' => 'bar',
    'c' => ['outro']
]

$array = json_decode('{ "a" : 1, "b" : [] }');
var_dump($array);
echo json_encore($array), "\n";

function soma($a, $b) {
    return $a + $b
}

$name = "World";
$helloWorld = function($greetings) use ($name) {
    echo $greetings, ' ', $name, '!!!';
};
$helloWorld('Hello');

// if, switch... while, do while, for, foreach...

$age = 25;
$result = match (true) {
    $age >= 65 => 'senior',
    $age >= 25 => 'adult',
    $age >= 18 => 'teen',
    default => 'kid'
}

$array = range(0,9);
foreach ($array as $value) {
    ...
}

for ($i = $start; $i <= $end; $i += $step) {
    yield $i; // alivia na memória
}

// require, require_once, include, include_once
    // include não falha se não encontra, require falha
    // once evita reimportar


