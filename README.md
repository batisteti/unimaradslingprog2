# Notas de Aula

Fábio Batisteti - RA 1952024  

## Formatação Markdown
- # título 1
- ## título 2

## Comandos Linux
shell > bash
bash "resumido": ash

cat: lê, não roda, o arquivo  
tail: pega o final do arquivo  
head: pega o começo do arquivo  
\>: redireciona a saída  
\<: redireciona a entrada  
\|: usa a saída como entrada  

ps: ver processos do sistema  
whoami: ver usuário  
id: ver detalhes do usuário  

nano: editor de texto  

vi: outro editor de texto  
    - escrever: i  
    - salvar: :w  
    - sair: :q  
    - salvar e sair: :wq  

chmod \[permissões\] \[arquivo\]  
Leitura: 4  
Escrita: 2  
Execução: 1  
Para User, Group e Others  

pwd: pega o diretório atual  
ls: lista conteúdo do diretório atual (-la: para listar com mais detalhes)  
cd: ir para diretório  
... / ou ~: home  
... ..: pasta anterior  

mkdir \[nome\]: cria pasta  

/: raíz  
/bin/: commandos binários essenciais  
/boot: boot loader  
/dev/: dispositivos  
/etc/: configurações do computador   
/home/: diretório de usuários  
/root: diretório do superusuário  
/tmp/: arquivos temporários  

## Containers

Comparado com máquinas virtuais, serve para ter um ambiente "comum" entre usuários  
Serve para criar ambientes com versões diferentes de programas e utilidades em uma mesma máquina  

    sudo dnf install -y docker

É necessário alterar um usuário para o grupo docker para poder alterar arquivos (id -un traz o usuário atual)

    sudo usermod -aG docker $(id -un)

Checar se o docker está funcionando (o -it acessa o terminal lá)

    docker run -it --rm hello-world

Rodar algo dentro do container

    docker run [imagem] [comando]
    docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:tag

--name é o nome do container, -e é uma variável de ambiente, -d para rodar no background  

Para listar os containers vivos (docker ps), ou mortos (... -a):

    docker ps -a

TUDÃO: http://dockerlabs.collabnix.com/docker/cheatsheet  

### Portas

Bridge: como se o computador fizesse um roteador e ligasse aos containers, e se comunicasse por ele  
Host: o container "assume o endereço do computador", então as requisições vão para ele ao invés do computador  

Redirecionando as resquisições de uma porta para outra (da 8080 para a 80), e conferir o IP

    docker run -itd --name web-server -p 8080:80 ngix
    docker inspect -f '{{ range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' web-server

Para matar o container

    docker rm -f web-server

### Volumes

Pasta no container que será guardada de forma diferente, que não some depois dele ser apagado

    docker run -d \
    --name some-postgress \
    -e POSTGRES_PASSWORD=mysecretpassword \
    -b /host/path:/var/lib/postgresql/data \
    postgres

    docker run -d \
    --name some-postgress \
    -e POSTGRES_PASSWORD=mysecretpassword \
    -b volume-name:/var/lib/postgresql/data \
    postgres

Dos dois de cima, é melhor utilizar o primeiro, que registra no caminho absoluto escrito

    docker run -d \
    --name some-mysql \
    -v /my/own/datadit:/var/lib/mysql \
    -e MYSQL_ROOT_PASSWORD=my-secret-pw \
    mysql:tag

### docker-compose

    pip install --user docker-compose

Depois de instalar, pode usar os comandos abaixo (trava terminal, não trava terminal)

    docker-compose up
    docker-compose up -d

O de cima não usa arquivo, se ele chamad docker-compose.yaml. Arquivo que será utilizado está no repositório

    docker-compose -f [caminho pro arquivo .yaml] up

## Instalando PHP

    sudo dnf install -y php php-json php-xml php-mbstring

E depois instalar o Composer (ver site).

## Banco no terminal

Para acessar o bash no container

    docker exec -it [nome] bash

Lá dentro, para acessar o client do banco, depois ele pede senha

    mysql -h [ip] -u [login] -p
    mysql -h 127.0.0.1 -u root -p 

